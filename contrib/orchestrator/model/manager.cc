#include"manager.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("Manager");
    NS_OBJECT_ENSURE_REGISTERED(Manager);

    
    TypeId Manager::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::Manager")
            .SetParent<Object>()
            .SetGroupName("MyManager")
            .AddConstructor<Manager>();
            
        return tid;
    }
    void Manager::GetTaskInfo(std::initializer_list<std::string> taskInfo)
    {
        m_generateUnit->GetInfo(taskInfo);

        Ptr<Task> task = m_generateUnit->Generate();
        std::cout<<1<<'\t';
        Time dt = task->GetGenerateTime();

        auto id = task->GetDestinationMId();
        if(id != "")
        {
            SendTask(task,dt);
        }
        else
        {
            Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
            if(dt>=temp->GetGenerateTime())
            {
            Time endTime = task->GetEndTime();

            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
            m_taskRelease.insert(it,task);
            Simulator::Schedule(dt,&Manager::RunTask,this);
            if(endTime<=Seconds(500))
            {
                Simulator::Schedule(endTime,&Manager::Release,this);

            }
            }
        }
    }
    void Manager::GetTaskInfo(void)
    {
            m_generateUnit->ContinuousDiscreteDistributions();//[ min, max)上的连续均匀分布生成Task
            for(auto task:m_generateUnit->m_task)
            {
		        Time dt = task->GetGenerateTime();
		
		        auto id = task->GetDestinationMId();
		        if(id != "")
		        {
		            SendTask(task,dt);
		        }
		        else
		        {
		            Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
		            if(dt>=temp->GetGenerateTime())
		            {
			            Time endTime = task->GetEndTime();
			            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
			            m_taskRelease.insert(it,task);
			            Simulator::Schedule(dt,&Manager::RunTask,this);
			            if(endTime<=Seconds(500))
			            {
			                Simulator::Schedule(endTime,&Manager::Release,this);
			            }
		            }
	            }
	    }
        
    }

    bool Manager::ReceiveTask(Ptr<Task> t)
    {
        auto success =  m_receiveUnit->ReceiveTask(t);
        Simulator::ScheduleNow(&Manager::RunTask,this);
        return success;

    }
    void Manager::SendTask(Ptr<Task> t,Time dt)
    {
        //默认client安装在0,server安装在1
        const Task task = *t; 
        auto client = DynamicCast<Client>(m_node->GetApplication(0));
        client->ReceiveTask(task,dt);
        
    }
    void Manager::RunTask(void)
    {
        m_orchestratorUnit->ChooseTask();
        m_orchestratorUnit->Run();
    }
    void Manager::ShowTaskStatus(void)
    {
        m_taskStatusTable->Print();
    }
    void Manager::Release()
    {
        Time t = Simulator::Now();
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[t](Ptr<Task> task){return  t<=task->GetEndTime();});
	    auto task = *(it);
        m_taskRelease.erase(it);
	    m_orchestratorUnit->ReleaseResource(task);

    }
    void Manager::SetNode(Ptr<Node> node)
    {
        m_node = node;
        m_orchestratorUnit = Create<DefaultOrchestrator>(m_taskStatusTable,m_node);
        m_generateUnit = Create<DefaultGenerate>(m_taskStatusTable,m_machineId,m_node);
        m_receiveUnit = Create<DefaultReceive>(m_taskStatusTable,m_machineId);
        m_sendUnit = Create<DefaultSend>();
    }
    Ptr<Node> Manager::GetNode(void) const
    {
        return m_node;
    }

} 
