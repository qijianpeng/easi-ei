#include"generatetaskbase.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("GenerateTaskBase");
    NS_OBJECT_ENSURE_REGISTERED(GenerateTaskBase);

    TypeId GenerateTaskBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::GenerateTaskBase")
            .SetParent<Object>()
            .SetGroupName("GenerateTask");

        return tid;
    }

}
