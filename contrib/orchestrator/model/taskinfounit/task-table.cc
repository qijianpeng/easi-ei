#include"task-table.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("TaskTable");
    NS_OBJECT_ENSURE_REGISTERED(TaskTable);
    void TaskTable::GetTask(Ptr<Task> t)
    {
        InsertByPriority(t);
    }
    void TaskTable::Print(void) const
    {
        std::cout<<"UnsubmitList information"<<std::endl;
        Printlist(m_unsubmitList);
        std::cout<<"PendingList information"<<std::endl;
        Printlist(m_pendingList);
        std::cout<<"RunningList information"<<std::endl;
        Printlist(m_runningList);
        std::cout<<"DeadList information"<<std::endl;
        Printlist(m_deadList);
    }
    bool TaskTable::CheckIn(Ptr<Task> t) const
    {
        auto it = find_if(m_pendingList.begin(),m_pendingList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
        if(it == m_pendingList.end())
        {
            std::cout<<"Task dosen't exist in this machine's Pending list,you can't schedule it "<<std::endl;
            abort();
            return false;//表示当前无可执行task
        }
        return true;
    }
    void TaskTable::PendingToRunning(Ptr<Task> t)
    {
        auto it = find_if(m_pendingList.begin(),m_pendingList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
        auto temp = *it;
        (*it)->Running();
        InsertByPriority(*it);
        m_pendingList.erase(it);

    }
    void TaskTable::RunningToDead(Ptr<Task> t)
    {
        auto it = find_if(m_runningList.begin(),m_runningList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
        if(it == m_pendingList.end())
        {
            std::cout<<"Task doesn't exist "<<std::endl;
        }
        (*it)->Dead();
        InsertByPriority(*it);
        m_runningList.erase(it);
    }
    void TaskTable::Printlist(const std::list<Ptr<Task>> &l) const
    {
        std::cout<<"TaskId"<<'\t'<<"cpu"<<'\t'<<"mem"<<'\t'<<"priority"<<'\t'<<"category"<<'\t'<<"time"<<std::endl;
        for(auto i = l.begin();i!=l.end();++i)
        {
            std::cout<<(*i)->GetTId()<<'\t'<<(*i)->GetRCpu()<<'\t'<<(*i)->GetRMemory()<<'\t'<<(*i)->GetPriority()<<'\t'<<'\t'<<(*i)->GetCategoryString()<<"\t"<<(*i)->GetGenerateTime()<<std::endl;
        }
    }
    Ptr<Task> TaskTable::FirstPriorInPending(void)
    {
        return *m_pendingList.begin();
    }
    void TaskTable::DeletePendingFirst(void)
    {
        m_pendingList.pop_front();
    }
    std::list<Ptr<Task>> TaskTable::GetPendingList(void) const
    {
        return m_pendingList;
    }
    bool TaskTable::PendingEmpty(void)
    {
        return m_pendingList.empty();
    }
    void TaskTable::InsertByPriority(Ptr<Task> t)
    {
        std::list<Ptr<Task>> *l = nullptr;
        int cate = t->GetCategoryInt();
        switch(cate)
        {
            case 1:
                {
                    l = &m_unsubmitList;
                    break;
                }
            case 2:
                {
                    l = &m_pendingList;
                    break;
                }
            case 3:
                {
                    l = &m_runningList;
                    break;
                }
            case 4:
                {
                    l = &m_deadList;

                    break;
                }
            default:
                {
                    std::cout<<"Error: Task status doesn't match any category"<<std::endl;
                    abort();
                }
        }
        auto it = find_if(l->begin(),l->end(),[t](Ptr<Task> a){return  a->GetGenerateTime()<t->GetGenerateTime();});
        l->insert(it,t);
        l->sort(Compare);
    }
    bool TaskTable::Compare(Ptr<Task> a,Ptr<Task> b)
    {
	        if(a->GetGenerateTime()==b->GetGenerateTime())
	        {
	            return a->GetPriority()<b->GetPriority();
	        }
	        else
	        {
	            return a->GetGenerateTime()<b->GetGenerateTime();
	        }
    }
}

