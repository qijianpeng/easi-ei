#ifndef MANAGER_H
#define MANAGER_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/task-table.h"
#include"ns3/default-generate.h"
#include"ns3/default-orchestrator.h"
#include"ns3/default-receive.h"
#include"ns3/default-send.h"
#include<initializer_list>
namespace ns3{
    class OrchestratorBase;
    class GenerateTaskBase;
    class DefaultGenerate;
    class Manager:public Object{ //Manager作为任务生成部件、接收、发送、调度部件的集合体。
        public:
            static TypeId GetTypeId(void);
            Manager()=default;
            void GetTaskInfo(std::initializer_list<std::string> taskInfo);//接收真实数据
            void GetTaskInfo(void);//按照概率分布生成数据
            bool ReceiveTask(Ptr<Task> t);
            void SendTask(Ptr<Task> t,Time dt);
            void RunTask(void);
            void ShowTaskStatus(void);
            void SetNode(Ptr<Node> node);
            void Release();
            Ptr<Node> GetNode(void) const;
        private:
            Ptr<TaskTable> m_taskStatusTable = CreateObject<TaskTable>();
            std::list<Ptr<Task>> m_taskRelease;//用于暂时记录程序运行中的task
            Ptr<Node> m_node;
            std::string m_machineId="";
            Ptr<DefaultOrchestrator> m_orchestratorUnit;
            Ptr<DefaultGenerate> m_generateUnit;
            Ptr<DefaultReceive> m_receiveUnit;
            Ptr<DefaultSend> m_sendUnit;
            Time m_dt;
            
    };
}

#endif /* MANAGER_H */
